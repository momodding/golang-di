package main

import (
	"fmt"
	"net"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gin-rest-di/todo"
	"gin-rest-di/user"
)

// var db *gorm.DB
func initDB() *gorm.DB {
	var err error

	addrs, err := net.LookupHost("docker0")
	if err == nil {
		for i := 0; i < len(addrs); i++ {
			fmt.Println(addrs[i])
		}
	}
 	//open a db connection
	db, err := gorm.Open("mysql", "root:password@tcp(172.17.0.1:3308)/go_gin_todo?charset=utf8mb4&parseTime=True&loc=Local")
	if err != nil {
		panic("failed to connect database")
	}

	//Migrate the schema
	db.AutoMigrate(&todo.TodoModel{})
	db.AutoMigrate(&user.UserModel{})

	return db
}

func main() {
	db := initDB()
	defer db.Close()

	todoApi, err := InitTodoController(db)
	if err != nil {
		panic("failed to inject dependency")
	}

	router := gin.Default()
	v1 := router.Group("/api/v1")
	{
		v1.POST("/login", LoginHandler)
		v1.POST("/todos/", todoApi.CreateTodo)
		v1.GET("/todos/", Auth, todoApi.FetchAllTodo)
		v1.GET("/todos/:id", todoApi.FetchSingleTodo)
		v1.PUT("/todos/:id", todoApi.UpdateTodo)
		v1.DELETE("/todos/:id", todoApi.DeleteTodo)
	}
 	router.Run()
}