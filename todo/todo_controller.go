package todo

import (
	"net/http"
	"strconv"
	"github.com/gin-gonic/gin"
)

type TodoController struct {
	TodoService TodoService
}

func ProvideTodoController(serv TodoService) TodoController {
	return TodoController{TodoService:serv}
}

// createTodo add a new todo
func (service *TodoController) CreateTodo(c *gin.Context) {
	completed, _ := strconv.Atoi(c.PostForm("completed"))
	todo := TodoModel{Title: c.PostForm("title"), Completed: completed}
	service.TodoService.SaveOrUpdate(todo)
	c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "Todo item created successfully!", "resourceId": todo.ID})
}

// fetchAllTodo fetch all todos
func (service *TodoController) FetchAllTodo(c *gin.Context) {
	var todos []TodoModel
	var _todos []TodoMap

	todos = service.TodoService.FindAll()

	if len(todos) <= 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No todo found!"})
		return
	}

	//transforms the todos for building a good response
	for _, item := range todos {
		completed := false
		if item.Completed == 1 {
			completed = true
		} else {
			completed = false
		}
		_todos = append(_todos, TodoMap{ID: item.ID, Title: item.Title, Completed: completed})
	}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": _todos})
}

// fetchSingleTodo fetch a single todo
func (service *TodoController) FetchSingleTodo(c *gin.Context) {
	var todo TodoModel
	todoID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		panic("failed to parse ID")
	}

	todo = service.TodoService.FindByID(todoID)

	if todo.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No todo found!"})
		return
	}

	completed := false
	if todo.Completed == 1 {
		completed = true
	} else {
		completed = false
	}

	_todo := TodoMap{ID: todo.ID, Title: todo.Title, Completed: completed}
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": _todo})
}

// updateTodo update a todo
func (service *TodoController) UpdateTodo(c *gin.Context) {
	var todo TodoModel
	todoID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		panic("failed to parse ID")
	}

	todo = service.TodoService.FindByID(todoID)

	if todo.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No todo found!"})
		return
	}

	todo.Title = c.PostForm("title")
	completed, _ := strconv.Atoi(c.PostForm("completed"))
	todo.Completed = completed

	service.TodoService.SaveOrUpdate(todo)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Todo updated successfully!"})
}

// deleteTodo remove a todo
func (service *TodoController) DeleteTodo(c *gin.Context) {
	var todo TodoModel
	todoID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		panic("failed to parse ID")
	}

	todo = service.TodoService.FindByID(todoID)

	if todo.ID == 0 {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No todo found!"})
		return
	}

	service.TodoService.Delete(todo)
	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Todo deleted successfully!"})
}