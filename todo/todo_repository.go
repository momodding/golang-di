package todo

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type TodoRepository struct {
	db *gorm.DB
}

func ProvideTodoRepository(db *gorm.DB) TodoRepository {
	return TodoRepository{db:db}
}

// createTodo add a new todo
func (repos *TodoRepository) Save(todo TodoModel) TodoModel {
	repos.db.Save(&todo)

	return todo
}

// fetchAllTodo fetch all todos
func (repos *TodoRepository) FindAll() []TodoModel {
	var todos []TodoModel

	repos.db.Find(&todos)
	return todos
}

// fetchSingleTodo fetch a single todo
func (repos *TodoRepository) FindById(id int) TodoModel {
	var todo TodoModel
	todoID := id
	repos.db.First(&todo, todoID)

	return todo;
}

// deleteTodo remove a todo
func (repos *TodoRepository) Delete(todo TodoModel) TodoModel {
	repos.db.Delete(&todo)

	return todo
}