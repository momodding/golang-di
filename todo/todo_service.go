package todo

type TodoService struct {
	TodoRepository TodoRepository
}

func ProvideTodoService(repo TodoRepository) TodoService {
	return TodoService{TodoRepository: repo}
}

func (serv *TodoService) FindAll() []TodoModel {
	return serv.TodoRepository.FindAll()
}

func (serv *TodoService) FindByID(id int) TodoModel {
	return serv.TodoRepository.FindById(id)
}

func (serv *TodoService) SaveOrUpdate(todo TodoModel) TodoModel {
	return serv.TodoRepository.Save(todo)
}

func (serv *TodoService) Delete(todo TodoModel) {
	serv.TodoRepository.Delete(todo)
}