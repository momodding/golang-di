package todo

func ToTodo(_todo TodoMap) TodoModel {
	completed := 0
	if _todo.Completed {
		completed = 1
	} else {
		completed = 0
	}
	return TodoModel{Title:_todo.Title, Completed:completed}
}

func ToTodoMap(todo TodoModel) TodoMap {
	completed := false
	if todo.Completed == 1 {
		completed = true
	} else {
		completed = false
	}
	return TodoMap{ID: todo.ID, Title:todo.Title, Completed:completed}
}

func ToTodoMaps(todos []TodoModel) []TodoMap {
	_todos := make([]TodoMap, len(todos))
	if len(todos) <= 0 {
		return _todos
	}

	//transforms the todos for building a good response
	for _, item := range todos {
		completed := false
		if item.Completed == 1 {
			completed = true
		} else {
			completed = false
		}
		_todos = append(_todos, TodoMap{ID: item.ID, Title: item.Title, Completed: completed})
	}

	return _todos
}