package user

type UserService struct {
	UserRepository UserRepository
}

func ProvideUserService(repo UserRepository) UserService {
	return UserService{UserRepository: repo}
}

func (serv *UserService) FindAll() []UserModel {
	return serv.UserRepository.FindAll()
}

func (serv *UserService) FindByID(id int) UserModel {
	return serv.UserRepository.FindById(id)
}

func (serv *UserService) FindByUsername(username string) UserModel {
	return serv.UserRepository.FindByUsername(username)
}

func (serv *UserService) SaveOrUpdate(user UserModel) UserModel {
	return serv.UserRepository.Save(user)
}

func (serv *UserService) Delete(user UserModel) {
	serv.UserRepository.Delete(user)
}