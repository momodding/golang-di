package user

import (
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type UserRepository struct {
	db *gorm.DB
}

func ProvideUserRepository(db *gorm.DB) UserRepository {
	return UserRepository{db:db}
}

func (repos *UserRepository) Save(user UserModel) UserModel {
	repos.db.Save(&user)

	return user
}

func (repos *UserRepository) FindAll() []UserModel {
	var users []UserModel

	repos.db.Find(&users)
	return users
}

func (repos *UserRepository) FindByUsername(username string) UserModel {
	var user UserModel
	// userID := id
	repos.db.Where(&UserModel{Username: username}).First(&user)

	return user
}

func (repos *UserRepository) FindById(id int) UserModel {
	var user UserModel
	userID := id
	repos.db.First(&user, userID)

	return user
}

func (repos *UserRepository) Delete(user UserModel) UserModel {
	repos.db.Delete(&user)

	return user
}