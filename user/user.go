package user

import (
	"github.com/jinzhu/gorm"
)

type (
	// UserModel describes a todoModel type
	UserModel struct {
		gorm.Model
		Username     string `json:"username"`
		Password	 string `json:"password"`
	}

	// transformedUser represents a formatted todo
	UserMap struct {
		ID        	 uint   `json:"id"`
		Username     string `json:"username"`
		Password	 string `json:"password"`
	}
)