package user

// import (
// 	"net/http"
// 	"strconv"
// 	"github.com/gin-gonic/gin"
// )

// type UserController struct {
// 	UserService UserService
// }

// func ProvideUserController(serv UserService) UserController {
// 	return UserController{UserService:serv}
// }

// // createTodo add a new todo
// func (service *UserController) CreateUser(c *gin.Context) {
// 	user := UserModel{Username: c.PostForm("username"), Password: c.PostForm("password")}
// 	service.UserService.SaveOrUpdate(user)
// 	c.JSON(http.StatusCreated, gin.H{"status": http.StatusCreated, "message": "User item created successfully!", "resourceId": todo.ID})
// }

// // fetchAllTodo fetch all todos
// func (service *UserController) FetchAllUser(c *gin.Context) {
// 	var users []UserModel
// 	var _users []UserMap

// 	users = service.UserService.FindAll()

// 	if len(users) <= 0 {
// 		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No user found!"})
// 		return
// 	}

// 	//transforms the todos for building a good response
// 	for _, item := range users {
// 		_users = append(_users, TodoMap{ID: item.ID, Username: item.Username, Completed: item.Password})
// 	}
// 	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": _users})
// }

// // fetchSingleTodo fetch a single todo
// func (service *UserController) FetchSingleTodo(c *gin.Context) {
// 	var user UserModel
// 	userID, err := strconv.Atoi(c.Param("id"))
// 	if err != nil {
// 		panic("failed to parse ID")
// 	}

// 	user = service.UserService.FindByID(todoID)

// 	if user.ID == 0 {
// 		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No user found!"})
// 		return
// 	}

// 	_user := UserMap{ID: user.ID, Title: user.Username, Completed: user.Password}
// 	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": _user})
// }

// // fetchUserByUsername fetch a single user
// func (service *UserController) FetchSingleUserByUsername(c *gin.Context) {
// 	var user UserModel

// 	user = service.UserService.FindByUsername(c.Param("username"))

// 	if user.ID == 0 {
// 		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No user found!"})
// 		return
// 	}

// 	_user := UserMap{ID: user.ID, Title: user.Username, Completed: user.Password}
// 	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "data": _user})
// }

// // updateTodo update a todo
// func (service *UserController) UpdateUser(c *gin.Context) {
// 	var user UserModel
// 	userID, err := strconv.Atoi(c.Param("id"))
// 	if err != nil {
// 		panic("failed to parse ID")
// 	}

// 	user = service.UserService.FindByID(userID)

// 	if user.ID == 0 {
// 		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No user found!"})
// 		return
// 	}

// 	user.Title = c.PostForm("user")
// 	user.Password = c.PostForm("password")

// 	service.UserService.SaveOrUpdate(user)
// 	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "User updated successfully!"})
// }

// // deleteTodo remove a todo
// func (service *UserController) DeleteUser(c *gin.Context) {
// 	var todo UserModel
// 	userID, err := strconv.Atoi(c.Param("id"))
// 	if err != nil {
// 		panic("failed to parse ID")
// 	}

// 	todo = service.UserService.FindByID(todoID)

// 	if todo.ID == 0 {
// 		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "No todo found!"})
// 		return
// 	}

// 	service.UserService.Delete(todo)
// 	c.JSON(http.StatusOK, gin.H{"status": http.StatusOK, "message": "Todo deleted successfully!"})
// }