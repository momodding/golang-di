package main

import (
	"github.com/jinzhu/gorm"
	"github.com/google/wire"
	"gin-rest-di/todo"
	"gin-rest-di/user"
)

func InitTodoController(db *gorm.DB) (todo.TodoController, error) {
	wire.Build(todo.ProvideTodoRepository, todo.ProvideTodoService, todo.ProvideTodoController)

	return todo.TodoController{}, nil
}

func InitUserController(db *gorm.DB) (user.UserService, error) {
	wire.Build(user.ProvideUserRepository, user.ProvideUserService)

	return user.UserService{}, nil
}